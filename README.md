# Deploying FastAPI Microservices on Azure Kubernetes Service (AKS) using Terraform

This repository contains all the necessary configurations and guidelines for deploying a bookstore application built with FastAPI on Azure Kubernetes Service (AKS) using Terraform. This project demonstrates setting up a robust Kubernetes cluster and deploying microservices that manage books and client data, utilizing an API Gateway for seamless service interaction.
![Project_architecture.png](Assets%2FProject_architecture.png)

## Repository Structure

- `TerraformScriptsAKS`: Contains Terraform scripts for provisioning the AKS cluster.
- `FastAPIApps`: Contains the FastAPI applications along with Dockerfiles and `requirements.txt` for each microservice.
- `assets`: This folder contains a complete list of commands used (`commands.txt`) and a PDF with step-by-step screenshots detailing the entire process.
- `deployments`: Kubernetes YAML configurations for deploying the microservices.

