variables:
  TF_ROOT: ${CI_PROJECT_DIR}/Terraform_Scripts_AKS  # The relative path to the root directory of the Terraform project
  TF_STATE_NAME: default  # Name of the Terraform state file
  KUBECONFIG: /root/.kube/config

include:
  - template: Jobs/SAST-IaC.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
stages:

  - validate
  - test
  - build
  - deploy
  - destroy_plan
  - destroy



terraform:validate:
  stage: validate
  image:
    name: mcr.microsoft.com/azure-cli:latest
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    # Install dependencies
    - apk update && apk add --no-cache curl unzip
    # Install Terraform
    - curl -Lo /tmp/terraform.zip https://releases.hashicorp.com/terraform/1.8.5/terraform_1.8.5_linux_amd64.zip
    - unzip /tmp/terraform.zip -d /usr/local/bin/
    - terraform --version
    # Login to Azure CLI using the service principal
    - az login --service-principal -u $AZURE_CLIENT_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN  
  cache:
    key: "${TF_ROOT}"
    paths:
      - ${TF_ROOT}/
  script:
    - cd ${TF_ROOT}
    - terraform init -upgrade
    - terraform fmt
    - terraform validate


terraform:plan:
  stage: build
  image:
    name: mcr.microsoft.com/azure-cli:latest
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    # Install dependencies
    - apk update && apk add --no-cache curl unzip
    # Install Terraform
    - curl -Lo /tmp/terraform.zip https://releases.hashicorp.com/terraform/1.8.5/terraform_1.8.5_linux_amd64.zip
    - unzip /tmp/terraform.zip -d /usr/local/bin/
    - terraform --version
    # Login to Azure CLI using the service principal
    - az login --service-principal -u $AZURE_CLIENT_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN  
  cache:
      key: "${TF_ROOT}"
      paths:
        - ${TF_ROOT}/    
  script:
    - cd ${TF_ROOT}
    - terraform init -upgrade
    - terraform init -backend-config="path=${TF_ROOT}/${TF_STATE_NAME}.tfstate"
    - terraform plan -out=tfplan
  artifacts:
    paths:
      - ${TF_ROOT}/tfplan
    expire_in: 1 day

terraform:apply:
  stage: deploy
  image:
    name: mcr.microsoft.com/azure-cli:latest
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    # Install dependencies
    - apk update && apk add --no-cache curl unzip
    # Install Terraform
    - curl -Lo /tmp/terraform.zip https://releases.hashicorp.com/terraform/1.8.5/terraform_1.8.5_linux_amd64.zip
    - unzip /tmp/terraform.zip -d /usr/local/bin/
    - terraform --version
    # Login to Azure CLI using the service principal
    - az login --service-principal -u $AZURE_CLIENT_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN  
  cache:
      key: "${TF_ROOT}"
      paths:
        - ${TF_ROOT}/  
  script:
    - cd ${TF_ROOT}
    - terraform apply -auto-approve tfplan
    - terraform output -json > ${TF_ROOT}/tf_output.json  # Create the output file in the project root directory
  artifacts:
    reports:
      terraform: ${TF_ROOT}/tf_output.json
  when: manual

terraform:destroy:plan:
  stage: destroy_plan
  image:
    name: mcr.microsoft.com/azure-cli:latest
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    # Install dependencies
    - apk update && apk add --no-cache curl unzip
    # Install Terraform
    - curl -Lo /tmp/terraform.zip https://releases.hashicorp.com/terraform/1.8.5/terraform_1.8.5_linux_amd64.zip
    - unzip /tmp/terraform.zip -d /usr/local/bin/
    - terraform --version
    # Login to Azure CLI using the service principal
    - az login --service-principal -u $AZURE_CLIENT_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN  
  cache:
      key: "${TF_ROOT}"
      paths:
        - ${TF_ROOT}/  
  script:
    - cd ${TF_ROOT}
    - terraform plan -destroy -out=tfdestroyplan
  artifacts:
    paths:
      - ${TF_ROOT}/tfdestroyplan
    expire_in: 1 day
  when: manual
  allow_failure: true

terraform:destroy:
  stage: destroy
  image:
    name: mcr.microsoft.com/azure-cli:latest
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  before_script:
    # Install dependencies
    - apk update && apk add --no-cache curl unzip
    # Install Terraform
    - curl -Lo /tmp/terraform.zip https://releases.hashicorp.com/terraform/1.8.5/terraform_1.8.5_linux_amd64.zip
    - unzip /tmp/terraform.zip -d /usr/local/bin/
    - terraform --version
    # Login to Azure CLI using the service principal
    - az login --service-principal -u $AZURE_CLIENT_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN  
  cache:
      key: "${TF_ROOT}"
      paths:
        - ${TF_ROOT}/  
  needs:
    - job: terraform:destroy:plan
      artifacts: true
  script:
    - cd ${TF_ROOT}
    - terraform apply -auto-approve tfdestroyplan
  when: manual
  allow_failure: true

deploy_microservices:
  stage: deploy
  needs:
    - job: terraform:apply
      artifacts: true
  image:
    name: mcr.microsoft.com/azure-cli:latest
    entrypoint: [""]
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com
  cache:
    key: "${TF_ROOT}"
    paths:
      - ${TF_ROOT}/
  before_script:
    # Prepare artifacts
    - mkdir -p ${CI_PROJECT_DIR}/artifacts
    - cp ${TF_ROOT}/tf_output.json ${CI_PROJECT_DIR}/artifacts/tf_output.json
    # Login to Azure CLI using the service principal
    - az login --service-principal -u $AZURE_CLIENT_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN
    # Set the Azure subscription
    - az account set --subscription $AZURE_SUBSCRIPTION_ID
    # Retrieve Terraform output
    - TF_OUTPUT=$(cat ${CI_PROJECT_DIR}/artifacts/tf_output.json)
    - KUBERNETES_CLUSTER_NAME=$(echo $TF_OUTPUT | jq -r '.kubernetes_cluster_name.value')
    - RESOURCE_GROUP_NAME=$(echo $TF_OUTPUT | jq -r '.resource_group_name.value')
    # Get AKS credentials
    - az aks get-credentials --resource-group $RESOURCE_GROUP_NAME --name $KUBERNETES_CLUSTER_NAME
    - az aks install-cli
    - kubectl version --client

  script: 
  - cd ${CI_PROJECT_DIR}/Terraform_Scripts_AKS/k8s/
  - kubectl create -f books-data-pvc.yml
  - kubectl create -f books-deployment.yml
  - kubectl create -f clients-data-pvc.yml
  - kubectl apply -f clients-deployment.yml
  - sleep 60 # Wait for 60 seconds
  - kubectl get deployments
  - kubectl get svc
  - kubectl get pods
  - kubectl get pods --show-labels
  - booksApiPod=$(kubectl get pods -l app=books,service=books-api -o jsonpath="{.items[0].metadata.name}")
  - echo $booksApiPod
  - kubectl exec $booksApiPod -- /bin/bash -c "df -h; ls /data"
  - kubectl cp ${CI_PROJECT_DIR}/books/app/data/books.csv $booksApiPod:/data/
  - kubectl exec $booksApiPod -- /bin/bash -c "ls /data"
  - cd ${CI_PROJECT_DIR}/
  - mkdir -p gateway-conf
  - cp gateway/app/conf.d/main.yml gateway-conf/
  - kubectl create cm books-gateway-conf --from-file=gateway-conf/
  - cd ${CI_PROJECT_DIR}/Terraform_Scripts_AKS/k8s/
  - kubectl create -f gateway-deployment.yml
  - sleep 30
  - kubectl get cm
  - kubectl get po
  - kubectl get deployments
  - kubectl get svc    
  - kubectl expose deploy books-api --port 8000
  - kubectl expose deploy clients-api --port 8000
  - kubectl expose deploy gateway-api --port 8000 --type LoadBalancer  
  - sleep 30
  - kubectl get svc  
